#!/bin/sh
DEST="/home/cmejia/Respaldos/Sparrow"
DIR=respaldo`date +"%Y-%m-%d"` 
MYSQL_USER=soporte 
MYSQL_PASS=".4C3r04dm1n"
MYSQL_SERVER="10.10.0.2"
MYSQL_CONN="-h${MYSQL_SERVER} -u${MYSQL_USER} -p${MYSQL_PASS}"
#
# Collect all database names except for mysql, information_schema, and
# performance_schema
#
SQL="SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN ('mysql','information_schema','performance_schema', 'sys')"
DBLISTFILE=${DEST}/DatabasesToDump.txt
mysql ${MYSQL_CONN} -ANe"${SQL}" > ${DBLISTFILE}

DBLIST=""
for DB in `cat ${DBLISTFILE}` ; do DBLIST="${DBLIST} ${DB}" ; done

#MYSQLDUMP_OPTIONS="-B -d --routines --triggers --single-transaction"
MYSQLDUMP_OPTIONS="--log-error --no-create-db --skip-add-drop-table --routines --triggers --single-transaction --column-statistics=0"
mysqldump ${MYSQL_CONN} ${MYSQLDUMP_OPTIONS} --databases ${DBLIST} | gzip -9 > ${DEST}/${DIR}.sql.gz

for DB in `cat ${DBLISTFILE}` ; do 
	mysqldump ${MYSQL_CONN} ${MYSQLDUMP_OPTIONS} ${DB} | gzip -9 > ${DEST}/${DB}-${DIR}.sql.gz;
done

find ${DEST} -type f -mtime +7 -name '*.gz' -delete
